These tests make sure that the parameters are looked up in the right location.

1. test name, not directory add the parameters
2. they match if they are surrounded by minus
3. they match if they start with a minus and end at the file name
4. they match if they are in subfolders
5. they do not match if they are in different folders
6. they inherit from folders above
7. they do not inherit from folders that are not in the tests folder
