# bashtest

[![Gitlab CI build and test status](https://img.shields.io/gitlab/pipeline-status/niccokunzmann/python-recurring-ical-events?branch=master)](https://gitlab.com/niccokunzmann/python-recurring-ical-events/-/jobs)

![](feature.png)

`bashtest` allows you to scale up your tests for command line applications.
Run the same tests several times under different conditions using parameters and
re-use code where needed with fixtures.

Inspired by `py.test`, `bashtest` is highly flexible, customizable,
well-documented, easy to use and a low entry barrier to testing
and test-driven development.

## Installation

You can install the latest version of bashtest using these commands:

Install in `~/.bashrc`:
```
wget -O- https://niccokunzmann.gitlab.io/bashtest/install.sh | bash
```
```
curl https://niccokunzmann.gitlab.io/bashtest/install.sh | bash
```

Install in `/usr/local/bin`:
```
( cd /usr/local/bin ; wget -O- https://niccokunzmann.gitlab.io/bashtest/bashtest.tar | tar -xf- )
```
```
( cd /usr/local/bin ; curl https://niccokunzmann.gitlab.io/bashtest/bashtest.tar | tar -xf- )
```

## Usage

You can run single test files:
```
bashtest tests/test-1
bashtest tests/test-1 tests/test-2
```

You can run all tests in a diretory and all its sub-directories:
```
bashtest tests/option-tests
```

You can run all tests in the `tests` directory:
```
bashtest
```

You can run all tests in one direcory but not its sub-directories:
```
bashtest tests/option-tests/test-*
```

## Project structure

This is a bashtest file structure for a simple example project.
Suppose, you write a script called `script.sh` which you want to test.
Next to the script, you can find the tests folder with several tests.

```
my-project
 +- script.sh
 +- tests
    +- test-install
    +- argument-tests
       +- test-help-works
       +- test-nothing-fails
```

Running bashtest in this folder creates this output:

```
$ bashtest my-project/tests
test-install OK
argument-tests/test-help-works OK
argument-tests/test-nothing-fails OK
TOTAL 3 SUCCESS 3 FAIL 0
```

A simple test could look like this:

```
01  # my-project/tests/argument-tests/test-help-works
02  
03  capture ../../script.sh --help
04  
05  assertEquals "$EXITCODE" 0 "script.sh --help should exit with 0."
06  assertIncludes "$STDOUT" "Help for script.sh:"
```
And this is what happens:

- In Line 3, the test runs `script.sh --help` and *capture*s the output.
- In Line 5, we make sure that the script exited with zero exit status.
- In Line 6, we check that there is a help message.

bashtest allows you to write small tests and it 
comes with many more assertions and features about which you can 
read in the following sections.

## Parametrizing Tests

You can run a test several times with different parameters.
This could be the project structure where the test
`test-with-PARAMETER` is run twice: once in `environment-1` and
once in `environment-2`

```
my-project
+- tests
   +- test-with-PARAMETER
   +- parameters
      +- PARAMETER
         +- environment-1
         +- environment-2
         +- .hidden-parameter
```

bashtest now runs these in order:
- `test-with-PARAMETER[environment-1]` with first `environment-1` and then `test-with-PARAMETER`
- `test-with-PARAMETER[environment-2]` with first `environment-2` and then `test-with-PARAMETER`
- `.hidden-parameter` is ignored as all parameter values with a preceding '.'.

It is recommended to set variables in the parameters:

```
# environment-1
PARAMETER=Alice
```

```
# environment-2
PARAMETER=Bob
```

### Parameter Variables

When a parameter is executed, it is executed in its directory.
These additional variables are set:

- `BASHTEST_PARAMETER`
  The path to the parameter value relative to the `BASHTEST_TESTS_DIRECTORY`.
  Example: `parameters/PARAMETER/environment-1`
- `BASHTEST_PARAMETER_NAME`
  The name of the parameter directory as used by the test cases for matching.
  Example: `PARAMETER`
- `BASHTEST_PARAMETER_VALUE`
  The name of the value file of the parameter.
  Example: `environment-1`
- `BASHTEST_PARAMETER_DIRECTORY`
  The absolute path to the directory of the parameter.
  This is equal to the output of `pwd` as the parameter is executed in this directory.
  Example: `$HOME/my-project/tests/parameters/PARAMETER`
- `BASHTEST_PARAMETER_PATH`
  The absolute path to the file of the parameter value.
  Example: `$HOME/my-project/tests/parameters/PARAMETER/environment-1`  

You can also access all the [`BASHTEST_TEST_*` variables](#test-variables).

## conftest - Sharing Functions

Many of your tests may require different functions again and again.
The file named `conftest` at the root of all your tests is sourced before all
test runs so the functions in it are shared.

See this example structure:

```
tests
 +- conftest
 +- test-1
 +- other-tests
     +- test-2
```

The file `conftest` is added once before all the tests.
So, the functions and variables defined in `conftest` can be used in both
`test-1` and `other-tests/test-2`.

If you require a cleanup after the tests are run,
you can define a `teardown` function as follows:

```
# conftest
function teardown() {
  # this is run after all tests
}
```

## Setup and Teardown

To keep the test code short, it is advisable to share common code that
runs before and after each test.
This project structure runs `setup` before each test and `teardown` after each test.

```
tests
 +- setup
 +- test-1
 +- test-2
 +- teardown
```

If you include parameters in the tests,
the setup function is run after the parameters.
Setups can be nested and count for each of the tests in subfolders, the same
with the teardowns:

```
tests
 +- setup
 +- test-1
 +- inner-tests
 |   +- setup
 |   +- test-2
 |   +- teardown
 +- teardown
```

`bashtest tests` results in this code being executed for `test-1`

- `tests/setup`
- `tests/test-1`
- `tests/teardown`

For `test-2`, the setups in the parent folder are inherited:

- `tests/setup`
- `tests/inner-tests/setup`
- `tests/inner-tests/test-2`
- `tests/inner-tests/teardown`
- `tests/teardown`

### Setup Variables

Since setup and teardown happen for many tests in different directories,
these variables can be used to get reliable behavior:

In `setup`:
- `BASHTEST_SETUP` is the relative path in the `BASHTEST_TESTS_DIRECTORY` to the
  `setup` script. 
- `BASHTEST_SETUP_PATH` is the absolute path to the `setup` script.
- `BASHTEST_SETUP_DIRECTORY` is the absolute path to the directory in which
  the setup script is located and run.
  This is also the same as `pwd`.

### Teardown Variables

In `teardown` in the same way:
- `BASHTEST_TEARDOWN` is the relative path in the `BASHTEST_TESTS_DIRECTORY` to the
  `teardown` script.
- `BASHTEST_TEARDOWN_PATH` is the absolute path to the `teardown` script.
- `BASHTEST_TEARDOWN_DIRECTORY` is the absolute path to the directory in which
  the `teardown` script is located and run.
  This is also the same as `pwd`.

## Speeding Up Test Execution by Storing Variables

It is tedious to run a long lasting command several times before each test
when its output does not change.
For this case, bashtest comes with the functions `store` and `restore`.
They store global variables between test runs
but each invocation of `bashtest` cannot access past stored values.

Consider this `setup` script, using the test helper `capture` to run a
script and store its output.

```
# setup
if ! restoreAll STDOUT EXITCODE
then
  capture wget -O- 'https://example.com'
  storeAll STDOUT EXITCODE
fi
```

In this case, setup is run before each test but `wget` retrieves the file only
once and the output is stored.

If you want to reuse the content of a [temporary directory](#temporary-directory), you could do this as well.

```
# setup

if ! restore temp_files
then
  store temp_files "`tempdir`"
fi
```

### Storage Functions

You can store values with `store`

- `store A` stores variable A and exports it.
- `store A value` stores variable A with the value `value` and exports it.

To store several variables at once, use `storeAll`.

- `storeAll A B C` stores all variables `A`, `B` and `C` and exports them.

Once variables are stored, they can be retrieved using `restore`.

- `restore unkown` fails if `store unkonwn` has not been used before.
- `restore A` exports `A` with the stored value of `A`.
- `restore A other_variable` exports `A` with the stored value of `A` and also exports `other_variable` with the same value as `A`.

To restore many variables at once, use `restoreAll`.

- `restoreAll A B C` exports `A` `B` `C` with their respective values.
  If one of the does not have a stored value, `restoreAll` fails after having
  restored all possible variables.

If you wish to remove a store value, you can do so with `unstore`.

- `unstore A` removes the stored value of `A`.
- `unstore A B C` removes the stored values of `A`, `B` and `C`.

## Fixtures

Fixtures are more sophisticated test parameters.
They may depend on each other, can be setup up and torn down and
parametrized themselves.

This is an example structure which starts an HTTPServer for some tests.

```
tests
 +- parameters
 |   +- HOSTNAME
 |       +- localhost
 |       +- example.com
 +- fixtures
 |   +- HTTPServer-HOSTNAME
 |       +- setup
 |       +- teardown
 +- test-retrieve-index-file-from-HTTPServer
 +- test-404-for-unknown-directory-HTTPServer
 +- test-wget-is-present
 +- conftest
```

Without looking at the code, this can be the intention behind the files:
- The `HOSTNAME` parameter changes which hostname the server expects
  to serve the files from. The fixture `HTTPServer` and all
  depending tests will be run twice using `localhost` and `example.com`.
- The `HTTPServer` fixture has a `setup` to start the server, storing
  its process id `$!` in a variable,
  possibly using `store` and `restore` to not start and stop the
  server for each test.
  When the tests are done using the server, its process is killed.
- The test `test-wget-is-present` does not use the fixture `HTTPServer` amd
  thus will not execute the fixture.
- The tests with `HTTPServer` in their names are each executed once
  for each `HOSTNAME`, resulting in four test executions.
- Common functions are stored in the `conftest` script and thus are
  available in all the parameters, fixtures and tests.

### Fixture File

A very simple fixture just consists of a file with its name.

```
tests
 +- test-FixtureFile
 +- fixtures
     +- FixtureFile
 
```

This file `FixtureFile` is used once before each test which has
`FixtureFile` in its name.

### Fixture Directory

A directory of fixtures allows more flexibility.

```
fixtures
 +- Fixture
     +- setup
     +- teardown
     +- file
```

`setup` is for execution before a test and `teardown` is for execution
after a test.
The file `file` can be used by this fixture.

### Fixture Dependencies

Fixtures may depend on other parameters and fixtures.
If fixtures depend on parameters, a new test is executed for each
of the parameters in use.
These parameters do not need to be mentioned in the test case.

Fixtures may depend on one or many other fixtures.
These will be executed so that the dependencies are met.
So, if `F1` depends on `F2`, when `F1` is used, first `F2` is executed, then
`F1`. The `teardown` happens in reverse order.

### Fixture Variables

- `BASHTEST_FIXTURE`
  The relative path inside the tests directory.
  Example: `fixtures/Fixture-Dependency-PARAMETER`
- `BASHTEST_FIXTURE_NAME`
  The name of the fixture without parameters and dependencies.
  Example: `Fixture`
- `BASHTEST_FIXTURE_DIRECTORY`
  The absolute path to the directory in which the fixture is stored.
  This is also the working directory when the fixture is executed.
  Examples: 
  - `$HOME/my-project/tests/fixtures` for a file fixture and
  - `$HOME/my-project/tests/fixtures/Fixture-Dependency-PARAMETER` for a fixture
    with `setup` and `teardown`.

Fixtures have access to the variables of the parameters and `conftest` but not
to the test `setup`.

## Execution order

- once conftest outside of the test scope
- for each test
  - all the parameter values
  - all fixtures
  - all `setup` files
  - the test file
  - the `teardown` files
- once the `teardown()` function defined in conftest

## Naming Rules

These are a few rules for naming the files in the project structure:
- Test cases start with `test-`
- Folders which contain tests end with `-tests`

If you obey these rules, you do not get in trouble:
- Parameters are written all upper case like `PARAMETER`
- Set the same variables in all parameters.
- Fixtures are written in camel case like `Fixture` or `FixtureName`.

## Assertions

Assertions provide a strutured way of testing and provide
messages if something does not work as expected.
If one assertion fails, the following ones will not be exeuted:
```
assert false
# this will not be executed
assert true
```

There are a variety of asssertions to choose from as commonly used in other
languages and frameworks, too.

### Commands as Assertions

Genrally any command that fails with a non-zero exit code
counts as a test failure.
Here are some examples:

```
# commands for test fail
cd non-existent-directory
false
COMMAND_NOT_EXISTING
test -f "non-existent-file"
```

### assert true

`assert $a [$message]`

This assertion works when the first argument `$a` equals `true`.

```
# OK
assert true

# FAIL
assert false
assert ""
assert "true "
```

Give a message:

```
# OK, no message printed
assert true "expected true"

# FAIL, "expected true" printed
assert false "expected true"
```

### assertEquals

`assertEquals $a $b [$message]`

Checks the first two parameters `$a` and `$b` for equality.
The message argument is optional and echoed on failure.


```
# OK
assertEquals apple apple
assertNotEquals apple pear

# FAIL with message
assertEquals apple pear
assertEquals apple pear "You should not compare these."
assertNotEquals apple apple "The apple does not fall far from the stem."
```

### assertIncludes

`assertIncludes $text $subtext [$message]`

This checks if the `$subtext` is included in `$text`.
The message argument is optional and echoed on failure.

```
# OK
assertIncludes "abc" "a"

# FAIL, with message
assertIncludes "abc" "z"
```

### Counting Occurences - assertCounts

`assertCount $text $subtext $count [$message]`

You test the `$count` of a `$substring` occuring in a `$text`.
The message argument is optional and echoed on failure.

```
assertCounts "a a a" "a" 3
assertCounts "1 2 3" "3" 1

assertNotCounts "a a a" "b" 3
```

### Variables

Several checks for variables are implemented and accessible in every test.

#### Global Variables

`assertIsExported variableName`

You can check if a variable is accessible by subprocesses.

```
# success
export A=1
assertIsExported A

# fail with message
B=2
assertIsExported B
```

#### Defined/Undefined Variables

`assertIsDefined variableName`

```
a=1

# success
assertIsDefined a
assertIsNotDefined b

# fail with message
assertIsDefined b
assertIsNotDefined a
```

### `grep` Assertions

`assertGrep $text arguments...`

To get a nice message for a `grep` test, you can use `assertGrep` and `assertNotGrep`.


```
# ok
assertGrep "asd" "a"
assertGrep "aaa" -E "a*"
assertNotGrep "asd" "z"

# fail with message
assertGrep "asd" "z"
assertNotGrep "test" -Ee '....' 
```

### `test` Assertions

```
asserTest arguments...
message="..." assertTest arguments...
```

The command line tool `test` allows comparisms and other nice helpers.
These come with a predefined error message.

```
# ok
assertTest -f "`tempfile`"
assertTest -d "`tempdir`"
assertNotTest -f "`tempfile`-does-not-exist"
assertNotTest -d "`tempdir`-does-not-exist"

# fail with message
assertTest 1 > 2
assertNotTest -f "`tempfile`"
assertNotTest -d "`tempdir`"
assertNotTest 1 < 2

message="additional message" assertTest 1 == 2
message="additional message" assertNotTest 1 == 1
```

### Checking Command Execution

```
assertCaptureSuccess <command> <arguments>...
assertCaptureFail    <command> <arguments>...
```

If you like to run commands and capture their output
while making sure that they have a certain exit code,
you can use `assertCaptureSuccess` for exit code `0` and
`assertCaptureFail` for any exit code that is not `0`.
This is a shortcut for the helper
[`capture`](#running-commands) and thus `$EXITCODE` and
`$STDOUT` can be used after running
`assertCaptureSuccess` or `assertCaptureFail`

```
# ok
assertCaptureSuccess echo "Hello World!"
assertCaptureFail test 1 == 2

# fail with message
assertCaptureSuccess false
assertCaptureFail true
```

## Helpers

Different functions make testing easier.
This includes string manipulation and command execution.

### Running Commands

Run a command and capture its standard output in `$STDOUT`
and exit code in `$EXITCODE`.
See also `assertCaptureSuccess` and `assertCaptureFail`.

```
capture echo "Hello World"

# OK
assertIncludes "$STDOUT" "Hello"
# FAIL
assertIncludes "$STDOUT" '!'
```

Checking the exit code:

```
capture true

assertEquals "$EXITCODE" 0
```

### Temporary Files

```
tempfile
tempfile <ending>
```

Create a temporary file which has a certain ending.
You can create temporary files which can be deleted automatically once the
all tests are finished.

```
# create a temporary file
file="`tempfile`"
# fill the file
echo -n "Hello World!" > "$file"
# check the file for its content
assertEquals "`cat \"$file\"`" "Hello World!"
```

If you need a certain ending of the file, you can specify it.

```
htmlFile="`tempfile '.html'`"
```

### Temporary Directory

```
tempdir
tempdir <name>
```

Create a temporary directory with an optional name.
Temporary directories can be created and will be deleted when the
tests are finished.

This creates a temporary directory and copies all contents of a folder
`testfiles` into the temporary `directory`.
This way, the test commands can modify the contents.
Optimally, this happens in the `setup` script.

```
directory="`tempdir`"
cp -r testfiles/* "$directory"
# perform tests that modify the files
```

### Helpful functions

- `generateTextNotIncluded "$text1" "$text2"` outputs a text that is not included in `$text1` and in which `$text2` is not included.
- `count "$text" "$subtext"` outputs the number of times that `$subtext` occurs in `$text`
- `replaceNewlines "$text" "$newlineReplacement"` replaces all newlines in `$text` with `$newlineReplacement`

## Variables

Some variables are available during testing.
A general convention is that
- all variables starting with `BASHTEST_`
  - if documented can be used and they are exported using the `export`
    command. You can use them safely.
  - if undocumented should not be used.
- All variables starting with `_BASHTEST` must not be used.

Here is an overview of usable variables:

- `BASHTEST` is the absolute path to the `bashtest` executable file.
- `BASHTEST_DIRECTORY` is the absolute path to the directory of the
  `bashtest` exeutable file.
- `BASHTEST_TESTS_DIRECTORY` is the path of the folder containing all tests and the `conftest` file if present.

### Test Variables

- `BASHTEST_TEST` is the relative path of the test file inside of the
  `BASHTEST_TESTS_DIRECTORY`, i.e. `test-1`.
- `BASHTEST_TEST_NAME` is the name of the test which is displayed,
  including parameter and fixture information.
- `BASHTEST_TEST_PATH` is the absolute path to the test file.
- `BASHTEST_TEST_DIRECTORY` is the absolute path to the directory of the
  test file.
- `BASHTEST_TEST_WORKING_DIRECTORY` has the same value as `BASHTEST_TEST_DIRECTORY`.
  This variable can be changed in parameters, fixtures and setup to change the
  working directory of the test run.
  If you would like to execute the test in the directory of a parameter,
  setup or fixture, you can add this line:
  ```
  export BASHTEST_TEST_WORKING_DIRECTORY="`pwd`"
  ```

Setup files, parameters, `conftest`, fixtures and tests are executed in
their own directory.
This allows for easy access to files and directories but it must be
considered that a file next to e.g. a parameter cannot be as easily
accessed and should be prefixed with the absolute `BASHTEST_PARAMETER_DIRECTORY`
or `BASHTEST_FIXTURE_DIRECTORY` for fixtures in case it should be used in the test.

### Command Line Options

If you run `bashtest` on the command line,
you can pass these arguments:

- `--help`
  This prints a small help message.
- `--print` or `BASHTEST_OPTION_PRINT=true`
  This prints the generated code for the tests.
- `--keep-temp` or `BASHTEST_OPTION_KEEP_TEMP=true`
  This does not delete the temporary directories after the tests have finished.
  This way, one can debug more easily.
- `--temp-dir=<PATH>` or `BASHTEST_OPTION_TEMP_DIR="<PATH>"`
  This sets the path for the temporary files and directories bashtest creates.
- `--version`
  This prints the version of bashtest.
- `--progress` or `BASHTEST_OPTION_PROGRESS=true`
  This prints one `.` for each collected test while the
  tests are collected. If you run a lot of tests,
  this helps pass the time and see that something is
  happening.

All arguments starting with `--` are available in
the tests starting with `BASHTEST_OPTION_` in capital
case even if they are not listed above.
Examples:
- `bashtest --my-option` makes `export BASHTEST_OPTION_MY_OPTION=true` available.
- `bashtest --directory=/tmp/test-dir` makes `export BASHTEST_OPTION_DIRECTORY='/tmp/test-dir'` available.

If you like to configure all `bashtest` runs to use a different temporary directory,
add this to your `$HOME/.bashrc`:
```
export BASHTEST_OPTION_TEMP_DIR="$HOME/bashtest-temp-dir"
```

## Development Setup

Clone the repository

```
git clone https://gitlab.com/niccokunzmann/bashtest
```

Run the tests

```
cd bashtest
./bashtest
```

### Major Refactoring

If you do a major refactoring of bashtest which breaks all its functionality,
you can test these in order:
- single-file-tests
- directory-tests
- return-value-tests
- conftest-tests
- parameter-tests
- helper-tests
- assertion-tests

### Adding Options

If you like to add new options like `--progress`,
this is the process:

- Document the option in the README file.
- Document the option in `bashtest --help`
- Create a new directory for the tests like `tests/option-tests/<option>-tests`
- Add at least these tests:
  - a test for the option being absent (default)
  - a test for the option being present
- Add the reset of the option to `tests/conftest` so that
  test runs are not influenced by user preferences and
  inner test runs do not change with options passed to
  `bashtest`.

## Dependencies

- bash
- basename
- dirname
- seq
- grep
- sed
- ls
- test

## Deployments

`bashtest` is tested with GitLab CI.
All tests in the `tests` folder are run by the CI.
Whenever the `master` branch has a successful build, this version
of `bashtest` is deployed to GitLab Pages.
The recommended installation scripts use GitLab Pages to retrieve the
latest version of `bashtest`.

## Related Work

If you require a small testing framework that has tests written in bash,
bashtest might be it.
Otherwise, have a look at more mature frameworks like shellspec.
Links:
- https://github.com/dodie/testing-in-bash
- https://github.com/shellspec/shellspec
