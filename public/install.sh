#!/bin/bash
#
# Install bashtest from the url.
#

set -e

url="https://niccokunzmann.gitlab.io/bashtest/bashtest.tar"
dir="$HOME/.local/bashtest"
bashrc="$HOME/.bashrc"

echo "---> creating $dir"
mkdir -p "$dir"
cd "$dir"


echo "---> downloading $url"
wget -O bashtest.tar "$url"

echo "---> extracting $url"
tar -xf bashtest.tar
rm bashtest.tar

echo "---> adding bashtest in $dir to \$PATH in $bashrc"
line="export PATH=\"\$PATH:`pwd`\""
if cat "$bashrc" | grep -qxe "$line"; then
  echo "Success: updated bashtest to latest version."
else
  (
    echo "# add bashtest to path"
    echo "$line"
  ) >> "$bashrc"
  echo "Success: installed bashtest in $dir"
  echo "Run this to get access:"
  echo "  source $bashrc"
fi

